"""
Created: 07/04/2020
:author: JoKl
:company: iPEK

Function:
TCP/IP Blocking mode Control, instead of the I2C Adress, TCP/IP is used for Network communication between a Host and the Raspberry

"""



import socket

from datetime import datetime
IP_Adress = "192.168.1.106"

class Data_Formatting():
    """"
    Class to Format the input TCP Stream
    """
    def __init__(self,Data_Stream,Stream_Count):
        """Constructor
        :param Data_Stream: TCP Stream as a string
        :param Stream_Count: Number of Streams, could be fup for more readouts than one Register
        """

        self.stream = Data_Stream
        self.Multi_Stream  = Stream_Count #set to 1 if more than one Register is received

    def Seperate_Stream(self,stream):
        """ Split the Input Stream by the Seperator ","
        :param stream: Input Stream as a String
        :return: the splitted Stream (is now a list)
        """
        return stream.split(",")

    def Seperate_Content(self):
        """

        :return: returns the Received Data Register Value
        """
        if self.Multi_Stream != 1: #more than one Stream, seperate Streams
            print("Multible Strings")
            Streams = self.stream.split(";")
            Streams.replace(";","")
            print("Stream Count:{},Data Streams:{}".format(len(Streams),Streams))
        else :
            Streams = self.stream.replace(";","")
            #print("Data Stream:{}".format(Streams))

        # Data Format: Data = str(1)+","+str(Register_Bank)+","+str(Adress)+","+str(Value)+","+str(i2c_adress)+";"
        elem = []
        elem = self.Seperate_Stream(Streams)
        print("Reg Seperated:{}".format(elem))
        #return elem[3]
        try:
              Reg_Value =  elem[3]
              print("Type Reg Value:",type(Reg_Value))
              print("Register:[{}],Register Value: [{}]".format(int(elem[2]),Reg_Value))
              return Reg_Value
        except ValueError:
              print ("Conversion failed")
              return -1



def Write_to_Client(Register_Bank,Adress,Value,i2c_adress=0x33):
    """Write to Client Frame Protocol,
    Format: Identifier (0-3) , Register_Bank (0x00-0x07) , Register_Adress (Adress) , Value , i2c_adress ;
    Seperator between subelements == ","
    Seperator between Streams == ";"

    Identifier: 0x00 fup
                0x01 for Transmitting and Writing to the specific Register
                0x02 for Reading from a specific Register
                0x03 fup

    :param Register_Bank: Register Source Group (Group A-H)
    :param Adress: Register Adress
    :param Value: value for specific register
    :param i2c_adress: I2C Adress, default is 0x33
    :return: none, fup <=> check if value was set and return True/false
    """

    sObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # TCP/IP connection
    sObject.connect((IP_Adress, 50000))  # ports can be used from 495.. something

    Data = str(1)+","+str(Register_Bank)+","+str(Adress)+","+str(Value)+","+str(i2c_adress)+";"
    print("Data to Send:",Data)
    sObject.send(Data.encode())
    sObject.close()



def Read_from_Client(Registergroup,Register,Identifier,i2c_Adress):# indentifier is used to select between reg. Data and Bootup Data
    """
    Read from a Specific Client adress, therefore the Identifier must be 0x02

    :param Registergroup: Register Source Group (Group A-H/0x00-0x07)
    :param Register: Register Adress
    :param Identifier:  0x02 for Reading from a specific Register
    :param i2c_Adress: I2C Adress, 0x33 is the Default
    :return: returns the Register Value
    """

    sObject = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sObject.connect((IP_Adress,50000))
    Data = str("2")+","+str(Registergroup)+","+str(Register)+","+str(0x00)+","+str(i2c_Adress)+";"
    print("Data to Send: "+Data)
    Reg_Value=-129
    try:
        sObject.send(Data.encode())
        #now all Registers are in one response
        Reg_Response = sObject.recv(20).decode() # String decoded
        print ("Data Received:{}".format(Reg_Response))
        if not Reg_Response:
            print("No Data Received!")
        else:
            Data_Object = Data_Formatting(Reg_Response,1)
            Reg_Value = Data_Object.Seperate_Content()
    except:
        print("Error occured")
    finally:
        sObject.close()
    return Reg_Value
