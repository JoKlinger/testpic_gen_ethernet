"""
    Created: 07/04/2020
    :author JoKl
    :company iPEK
-------------------
Testpicture GUI for PC9030K Camera module
------------------
"""

from tkinter import *
from tkinter.ttk import *
from PIL import Image
from tkinter import filedialog
from tkinter import messagebox

#own
from PC9030K import *
from PC9030K_Help import *
from Remote_Control import *


# Store all Parameters
def Save_As_File(self):
    print(type(self))
    try:
        Storage_Path = filedialog.asksaveasfilename(initialdir="/home/pi/Desktop", title="Select File")
    except:
        print("Error Opening File")

    if Storage_Path is "":
        print("Storage Path not valid")
        return
    else:
        print("File generated in: ", Storage_Path)
        
        Burst_Class    = self.frames["Page_2"]
        Encad_Class    = self.frames["Page_3"]
        Pedestal_Class = self.frames["Page_4"]
        HSync_Class    = self.frames["Page_5"]
        
        Storage_Parameters = ["Sync Rise Time",Burst_Class.Get_Sync_Rising_Scale_Value(), "Burst Offset", Burst_Class.Get_Burst_Offset_Scale_Value(),
                              "Encad Rise Time", Encad_Class.Get_Encad_Rising_Scale_Value(), "Setup Width", Encad_Class.Get_Setup_Width_Scale_Value(),
                              "Pedestal", Pedestal_Class.Get_Pedestal_Scale_Value(),"Burst Slope",Pedestal_Class.Get_Burst_Slope_Scale_Value()]
  
        Store_Config(Storage_Parameters, Storage_Path)

# Input is a list definded as:

#   [Index]          [Index+1]
#Parameter Name   Parameter Value
def Store_Config(Storage_Parameters, Storage_Path):
    Param_Length = len(Storage_Parameters)
    print(Param_Length)

    file = open(Storage_Path, "w")

    for i in range(0, Param_Length - 1, 2):
        file.write(Storage_Parameters[i] + ":" + str(Storage_Parameters[i + 1]) + "\n")
    file.close()

def Open_Config_File(self):
    global sObject
    Open_Path = ""
    try:
        Open_Path = filedialog.askopenfilename(initialdir="/home/pi/Desktop", title="Select File")
        print("Selected Path:", Open_Path)
    except:
        print("Path not valid!")

    if Open_Path != "":

        f_ptr = open(Open_Path, "r")
        Lines = f_ptr.readlines()

        TMP_Reg = []
        Config_Settings = []
        i = 0
        for line in Lines:
            TMP_Reg = line.split(":")
            print(TMP_Reg)
            Config_Settings.append(TMP_Reg[0])
            Config_Settings.append(TMP_Reg[1])
        print("Config Data:")
        print(Config_Settings)
        
        # overcomplicated sh...
        Match_Found = False 
        
        for save_keys in Register_Selection:
            Match_Found = False
            print(save_keys)
            for i in range(0, len(Config_Settings), 2):
                if save_keys == Config_Settings[i]:
                    Register = int(Register_Selection.get(save_keys))
                    #print("Selected Reg",save_keys,type(save_keys))
                    Reg_Content = int(Config_Settings[i + 1])
                    print("Match Found, at Index:", i, "Register:", Register, "Register Content:", Reg_Content)
                    Write_to_Client(Group_Selection.get("Group H"),Register,Reg_Content,0x33)
                    Match_Found = True
                    break
                elif Match_Found == False and i==len(Config_Settings):
                    print("Open Config was not successfull")
                    print("Failed to match",Config_Settings[i])
        Update_Slider_Values(self)

def Disp_Picture(self):
    print("Testpicture Selected!")
    Testpicture = self.pic_list.curselection()  # Number of the selected Testpicture
    Write_Test_Pattern(Testpicture[0])  # function needs to be implemented


def Update_Slider_Values(self):
    Burst_Class = self.frames["Page_2"]
    Encad_Class = self.frames["Page_3"]
    Pedestal_Class = self.frames["Page_4"]
    HSync_Class = self.frames["Page_5"]

    HSync_Class.Set_HSync_Value_from_Register()

    Pedestal_Class.Set_Burst_Slope_Value_From_Register()
    Pedestal_Class.Set_Pedestal_Value_From_Register()

    Encad_Class.Set_Encad_Rising_Value_From_Register()
    Encad_Class.Set_Setup_Width_Value_From_Register()

    Burst_Class.Set_Burst_Offset_Value_From_Register()
    Burst_Class.Set_Sync_Rising_Value_From_Register()


#Flip Display 180°
def Flipping():
    Flip_Display(0x33, True)

#undo Flipping
def Flipp_Back():
    Flip_Display(0x33, False)

def Mirroring():
    Mirror_Display(0x33, True)
def Mirror_Undo():
    Mirror_Display(0x33, False)



#read from the given Register
def Get_Reg_Content(Register_Group_Input,Register_Input,Dest_Entry):
    try:
        Source_Bank = Register_Group_Input.get()
        Register = Register_Input.get()
        print("Source Bank", Source_Bank, "Register", Register)

        Register_Content = Read_from_Register(int(Source_Bank), int(Register))
        Dest_Entry.delete(0, 'end')
        Dest_Entry.insert(0, str(Register_Content))
    except:
        print("Nice Try")
        messagebox.showerror("Error","because of YOU, I have to write exceptions!\n Please Enter a valid number")

# write to specific Register
def Write_to_Register(Register_Source_Bank,Register_Entry,Register_Input_Write):
    Source_Bank = int(Register_Source_Bank.get())
    Register = int(Register_Entry.get())
    Register_Value = int(Register_Input_Write.get())
    print("Source Bank: {:x}, Register:{:x}, Register Value: {:x}".format(Source_Bank,Register,Register_Value))
    Write_to_Client(Source_Bank, Register, Register_Value,0x33)
# Change Slider Value manually
# third param is the Register String, which is defined by the Dictionary
def Change_Slider_Value_From_Input(entry1,scale,Reg_Str):
    try: 
        val = int(entry1.get())
        print("Value Changed to",val)
        scale.set(val)
        #Find matching Register to Update Camera Register
        Find_Matching_Register(Reg_Str,val)
    except:
        print("Nice Try")
        messagebox.showerror("Error","because of YOU, I have to write exceptions!\n Please Enter a valid number")
    

# Slider Value needs to be set inside camera
def Find_Matching_Register(Reg_Str,Reg_Value):
    for elem in Register_Selection:
        if elem == Reg_Str:
            Reg_Adress = int(Register_Selection.get(elem))
            print("Match Found,Adress:",hex(Reg_Adress))
            Write_to_Client(0x07,Reg_Adress,Reg_Value,0x33)
            break
    
#---------------------------------------------------------------------------------------------------
def Bootup_Check():
    pass


#----------------------------------------------------------------------------------------------------
def Close_Application(root):
    root.destroy()
    
#Help Menu, called via Button Press => displays picture for each setting

def Testpic_Help_Pic(root):
    print ("Testpic Help")
    Show_Testpicture_Help(root)

def Burst_Help_Pic(root):
    Show_Burst_Offset_Help(root)
    
def Sync_Help_Pic(root):
    Show_Rising_Help(root)
    
def Encad_Rising_Pic(root):
    Show_Encad_Rising_Help(root)
    
def Setup_Width_Pic(root):
    Show_Setup_Width_Help(root)
    
def Pedestal_Help_Pic(root):
    Show_Pedestal_Help(root)
    
def Burst_Slope_Help(root):
    Show_Burst_Slope_Help(root)
    
def Hsync_Pic(root):
    Show_Hsync_Pic_Help(root)

#----------------------------------------------------------------------------------------------------
#Main Class defines frame subclasses for sliders
class Testpicture_Gen(Tk):
      global Gui_Object
      def __init__(self, *args, **kwargs):
          Tk.__init__(self, *args, **kwargs)

          self.title("Testbildgenerator")
          self.geometry("800x400")
          self.resizable(False,False)
          self.style = Style()
          self.style.theme_use("alt")

          containter = Frame(self)
          containter.grid(row = 0,column =0)
          
        #---------------------------------------
          menu1 = Menu(self,font=14)

          # basic menu for Storage and quitting
          filemenue = Menu(menu1, tearoff=0,font = 14)
          filemenue.add_command(label="Open", command=lambda :Open_Config_File(self))
          filemenue.add_separator()
          filemenue.add_command(label="Save As", command=lambda : Save_As_File(self))
          filemenue.add_separator()
          filemenue.add_command(label="Quit", command=lambda:Close_Application(self))

          menu1.add_cascade(label="Filemenu", menu=filemenue,font = 14)

          Basic_cam_op = Menu(menu1, tearoff=0,font = 14)
          Basic_cam_op.add_command(label="Flip Display", command=Flipping)  # command=Flip_Display
          Basic_cam_op.add_separator()
          Basic_cam_op.add_command(label="Undo Flipping", command=Flipp_Back)  # command=Flip_Display
          Basic_cam_op.add_separator()
          Basic_cam_op.add_command(label="Mirror Display", command=Mirroring)  # command=Mirror_Display
          Basic_cam_op.add_separator()
          Basic_cam_op.add_command(label="Undo Mirroring", command=Mirror_Undo)  # command=Mirror_Display
          Basic_cam_op.add_separator()
          menu1.add_cascade(label="Mirror/ Flip", menu=Basic_cam_op,font = 14)

          #Menu swapping
          Camera_Settings_Menu = Menu(menu1,tearoff = 0,font = 14)
          Camera_Settings_Menu.add_command(label = "Testpicture Selection", command = lambda:self.show_frame("Page_1") )
          Camera_Settings_Menu.add_separator()
          Camera_Settings_Menu.add_command(label = "Burst_offset/Sync Risetime", command = lambda:self.show_frame("Page_2") )
          Camera_Settings_Menu.add_separator()
          Camera_Settings_Menu.add_command(label = "Encad_Rising/Setup Width ", command = lambda: self.show_frame("Page_3") )
          Camera_Settings_Menu.add_separator()
          Camera_Settings_Menu.add_command(label = "Pedestal/Burst Slope", command = lambda: self.show_frame("Page_4") )
          Camera_Settings_Menu.add_separator()
          Camera_Settings_Menu.add_command(label = "Hsync/", command = lambda: self.show_frame("Page_5") )
          Camera_Settings_Menu.add_separator()
          
          menu1.add_cascade(label="Video Settings",menu = Camera_Settings_Menu,font = 14)
            
          #Read or Write To Register
          Read_Write_Menu = Menu(menu1,tearoff = 0,font = 14)
          Read_Write_Menu.add_command(label="Read or Write",command = lambda:self.show_frame("Page_6"))
          menu1.add_cascade(label="Read and Write Reg content",menu = Read_Write_Menu,font = 14)

          # Update Slider Values, needed if TCP/IP Communication is active
          Update_Slider_Val = Menu(menu1, tearoff=0, font=14)
          Update_Slider_Val.add_command(label="Update Slider Values", command=lambda: self.Update_Slider_Values())
          menu1.add_cascade(label="Update Slider's", menu=Update_Slider_Val, font=14)
          self.config(menu=menu1)


          self.config(menu=menu1)
        #-------------------------------------

          self.frames ={}#generate dictionary for all subclasses (sub Frames)

          # Define all Subclasses <=> Submenus
          for F in (Page_1,Page_2,Page_3,Page_4,Page_5,Page_6):
              page_name = F.__name__# get class name
              frame = F(parent = containter, controller = self)
              self.frames[page_name] = frame         #define Dictionary for each class
              frame.grid(row=0,column=0,sticky="nsew")
        
          self.show_frame("Page_1")


      def Get_Page_Reference(self,page_class_name):#needed to access tkinter variables
          return self.frames[page_class_name]


      def show_frame(self,page_name):
          print("changed to:",page_name)
          frame = self.frames[page_name]
          frame.tkraise()

      def Update_Slider_Values(self):
          """
          Set Slider Values by reading from Raspberry Pi via TCP/IP
          :return: none
          """
          Page_2_Ref = self.frames["Page_2"]
          Page_2_Ref.Set_Burst_Offset_Value_From_Register()
          Page_2_Ref.Set_Sync_Rising_Value_From_Register()

          Page_3_Ref = self.frames["Page_3"]
          Page_3_Ref.Set_Encad_Rising_Value_From_Register()
          Page_3_Ref.Set_Setup_Width_Value_From_Register()

          Page_4_Ref = self.frames["Page_4"]
          Page_4_Ref.Set_Pedestal_Value_From_Register()
          Page_4_Ref.Set_Burst_Slope_Value_From_Register()

          Page_5_Ref = self.frames["Page_5"]
          Page_5_Ref.Set_HSync_Value_from_Register()

class Page_1(Frame):
       global query
       
       def __init__(self,parent,controller):
           Frame.__init__(self,parent)
           self.controller=controller
 
           # Buttons to switch between menus
           # -------------------------------------------------

           textpic_title = Label(self, text = "Testpicture Selection")
           textpic_title.grid(row=2, column=0)

           Textpicture_Scrollbar = Scrollbar(self, width=25, activebackground="red")
           Textpicture_Scrollbar.grid(row=4, column=1)

           self.pic_list = Listbox(self, yscrollcommand=Textpicture_Scrollbar.set, font=12, fg="blue",highlightcolor="LightSteelBlue3")

           for line in range(11):
               self.pic_list.insert(END, "Testpicture Nr:" + " " + str(hex(line)))

           self.pic_list.insert(END, "Testpicture Nr:" + " " + str(hex(0x0D)))
           self.pic_list.insert(END, "Testpicture Nr:" + " " + str(hex(0x0E)))
           self.pic_list.insert(END, "Testpicture Nr:" + " " + str(hex(0x0F)))

           for line in range(16, 29):
               self.pic_list.insert(END, "Testpicture Nr:" + " " + str(hex(line)))

           self.pic_list.insert(END, "Remove Testpicture")
           self.pic_list.grid(row=4, column=0)

           Textpicture_Scrollbar.config(command=self.pic_list.yview)
           Textpic_Sel_Button = Button(self, text="View Testpicture", command=lambda : Disp_Picture(self),
                                       activebackground="LightSteelBlue3")
           Textpic_Sel_Button.grid(row=10, column=0)

           # Window for Status Error output
           Error_Label = Label(self,font=14,text="Status Indication:")
           Error_Label.grid(row=3,column=3)
           Error_Output = Text(self,font=14,height =5 ,width =30)
           Error_Output.insert(END,query)
           Error_Output.grid(row = 4, column =3)

           # Help Menu

           Testpic_Help_Button = Button(self,text="Get Param Help 1",command = lambda :Show_Testpicture_Help(self,1),font=14)
           Testpic_Help_Button.grid(row=4,column=4)
           Testpic_Help_Button2 = Button(self,text="Get Param Help 2",command = lambda :Show_Testpicture_Help(self,2),font=14)
           Testpic_Help_Button2.grid(row=5,column=4)


class Page_2(Frame):

        def __init__(self,parent,controller):
           Frame.__init__(self,parent)
           self.controller=controller
           
           # Buttons to switch between menus
           # -------------------------------------------------
           # Burst settings

           Burst_Text = Label(self, text="Burst Offset [190ns/lsb]",font=14)
           Burst_Text.grid(row=3, column=0)
           
           self.Burst_Offset_Value = IntVar()
           self.Burst_Offset_Scale = Scale(self, from_=-128, to_=127,variable = self.Burst_Offset_Value ,orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
           self.Burst_Offset_Scale.grid(row=4, column=0)
           # Button for sending

           Burst_Button = Button(self, text="Burst Offset", command=lambda : Update_Burst_offset(self),activebackground="LightSteelBlue3",font=14)
           Burst_Button.grid(row=5, column=0)
           # Burst Checkbox for Direction change
           #Get_Direction = IntVar()
           #Direction = Checkbutton(self, text="Shift Right", variable=Get_Direction,font=14).grid(row=3, column=1)

           #Sync Risetime
           Sync_Text = Label(self, text="Sync Risetime [32ns/lsb]",font=14)
           Sync_Text.grid(row=8, column=0)

           self.Sync_Rising_Value = IntVar()
           self.Sync_Rising_Scale = Scale(self, from_=0, to_=16,variable = self.Sync_Rising_Value ,orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
           self.Sync_Rising_Scale.grid(row=9, column=0)

           # Button for sending
           Sync_Button = Button(self, text="Sync Risetime", command=lambda : Update_Sync_Risetime(self),
                                activebackground="LightSteelBlue3",font=14)
           Sync_Button.grid(row=10, column=0)

           Burst_Help_Button = Button(self,text = "Get Param Help",command = lambda : Burst_Help_Pic(self),font=14).grid(row = 4, column =3)
           Sync_Help_Button  = Button(self,text = "Get Param Help",command = lambda : Sync_Help_Pic(self),font=14).grid(row = 9, column =3)
           
           #Entry for manual input
           Burst_Usr_Entry = Entry(self,font=14,width=3)# Max 127
           Burst_Usr_Entry.grid(row = 4, column = 2)
           Burst_Usr_Button = Button(self,font=14,text="Change Burst Offset",
                                     command = lambda: Change_Slider_Value_From_Input(Burst_Usr_Entry,self.Burst_Offset_Scale,"Burst Offset"))
           Burst_Usr_Button.grid(row=5,column=2)

           Sync_Usr_Entry = Entry(self,font=14,width=2)
           Sync_Usr_Entry.grid(row=9, column=2)
           Sync_Usr_Button = Button(self, font=14, text="Change Sync",
                                     command=lambda: Change_Slider_Value_From_Input(Sync_Usr_Entry, self.Sync_Rising_Scale,"Sync Rise Time"))
           Sync_Usr_Button.grid(row=10, column=2)
           
           #Read Data from Registers
           Burst_Offset_Value =  Read_from_Register(0x07,Register_Selection.get("Burst Offset"))
           Sync_Risetime_Value = Read_from_Register(0x07, Register_Selection.get("Sync Rise Time"))
           
           #Set Slider Values
           self.Burst_Offset_Scale.set(Burst_Offset_Value)
           self.Sync_Rising_Scale.set(Sync_Risetime_Value)        
        
        def Set_Burst_Offset_Value_From_Register(self):
           Burst_Offset_Value = Read_from_Register(0x07,Register_Selection.get("Burst Offset"))
           if (Burst_Offset_Value > 127):
               Burst_Offset_Value = (Burst_Offset_Value -127)*(-1)
           self.Burst_Offset_Scale.set(Burst_Offset_Value)
           
        def Set_Sync_Rising_Value_From_Register(self):
           Sync_Risetime_Value = Read_from_Register(0x07, Register_Selection.get("Sync Rise Time"))
           print("Sync_Risetime_type:",type(Sync_Risetime_Value))
           self.Sync_Rising_Scale.set(Sync_Risetime_Value)

        def Get_Burst_Offset_Scale_Value(self):
           return self.Burst_Offset_Scale.get()

        def Get_Sync_Rising_Scale_Value(self):
           return self.Sync_Rising_Scale.get()
        
        def Set_Burst_Offset_Scale_Value(self,value):
           self.Burst_Offset_Scale.set(value)

        def Set_Sync_Rising_Scale_Value(self,value):
            if value > 15:
                value =15
            self.Sync_Rising_Scale.set(value)
           

class Page_3(Frame):

        def __init__(self, parent, controller):
            Frame.__init__(self, parent)
            self.controller = controller
            #-------------------------------------------------

            # Encad Rising Settings (4 Bit)
            Encad_Text = Label(self, text="Encad Rising",font = 14)
            Encad_Text.grid(row=3, column=0)

            self.Encad_Rising_Value = IntVar()
            self.Encad_Rising_Scale = Scale(self, from_=0, to_=16,variable = self.Encad_Rising_Value ,orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
            self.Encad_Rising_Scale.grid(row=4, column=0)

            # Button for sending
            Encad_Button = Button(self, text="Set Encad Rise Time", command=lambda : Update_Encad_Rising(self),
                                  activebackground="LightSteelBlue3",font = 14)
            Encad_Button.grid(row=5, column=0)

            #Setup Width
            Setup_Width_Text = Label(self, text="Setup Width [37ns/lsb]",font = 14)
            Setup_Width_Text.grid(row=8, column=0)

            self.Setup_Width_Value = IntVar()
            self.Setup_Width_Scale = Scale(self, from_=0, to_=32, variable = self.Setup_Width_Value,orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
            self.Setup_Width_Scale.grid(row=9, column=0)

            # Button Setup_Width_Scale sending
            Setup_Width_Button = Button(self, text="Setup Width", command=lambda : Update_Setup_Width(self),
                                        activebackground="LightSteelBlue3",font = 14)
            Setup_Width_Button.grid(row=10, column=0)

            #Manual User Input
            Encad_Usr_Entry = Entry(self, font=14, width=2)
            Encad_Usr_Entry.grid(row=4, column=2)
            Encad_Usr_Button = Button(self, font=14, text="Change Encad Offset",
                                      command=lambda: Change_Slider_Value_From_Input(Encad_Usr_Entry, self.Encad_Rising_Scale,"Encad Rise Time"))
            Encad_Usr_Button.grid(row=5, column=2)

            Setup_W_Usr_Entry = Entry(self, font=14, width=2)
            Setup_W_Usr_Entry.grid(row=9, column=2)
            Setup_W__Usr_Button = Button(self, font=14, text="Change Setup Width",
                                     command=lambda: Change_Slider_Value_From_Input(Setup_W_Usr_Entry, self.Setup_Width_Scale,"Setup Width"))
            Setup_W__Usr_Button.grid(row=10, column=2)



            # Help Menu Buttons
            Encad_Help_Button = Button(self,text = "Get Param Help",command = lambda:Encad_Rising_Pic(self),font=14).grid(row = 4, column =3)
            Setup_Help_Button  = Button(self,text = "Get Param Help",command = lambda:Setup_Width_Pic(self),font=14).grid(row = 9, column =3)
            
            
            #Read Data from Registers
            Encad_Value = Read_from_Register(0x07,Register_Selection.get("Encad Rise Time") )
            Setup_Width_Value = Read_from_Register(0x07, Register_Selection.get("Setup Width"))
           
            #Set Slider Values
            self.Encad_Rising_Scale.set(Encad_Value)
            self.Setup_Width_Scale.set(Setup_Width_Value)
           
            
        def Set_Encad_Rising_Value_From_Register(self):
            Encad_Value = Read_from_Register(0x07,Register_Selection.get("Encad Rise Time") )
            self.Encad_Rising_Scale.set(Encad_Value)
        
        def Set_Setup_Width_Value_From_Register(self):
            Setup_Width_Value = Read_from_Register(0x07, Register_Selection.get("Setup Width"))
            self.Setup_Width_Scale.set(Setup_Width_Value)
            
        def Get_Encad_Rising_Scale_Value(self):
            return self.Encad_Rising_Scale.get()
        def Get_Setup_Width_Scale_Value(self):
            return self.Setup_Width_Scale.get()

        def Set_Encad_Rising_Scale_Value(self,value):
            if value > 15 :
                value=15
            self.Encad_Rising_Scale.set(value)
            
        def Set_Setup_Width_Scale_Value(self,value):
            if value >31:
                value =31
            self.Setup_Width_Scale.set(value)    
            
            
class Page_4(Frame):

    def __init__(self, parent, controller):
        Frame.__init__(self, parent)
        self.controller = controller
        # -------------------------------------------------
        # pedestal
        # ------------------
        Pedestal_Text = Label(self, text="Pedestal",font=14)
        Pedestal_Text.grid(row=3, column=0)

        self.Pedestal_Value = IntVar()
        self.Pedestal_Scale = Scale(self, from_=0, to_=255, variable = self.Pedestal_Value ,orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
        self.Pedestal_Scale.grid(row=4, column=0, sticky=W, pady=2)

        # Button Pedestal sending
        Pedestal_Button = Button(self, text="Set Pedestal", command=lambda : Update_Pedestal(self),activebackground="LightSteelBlue3",font = 14)
        Pedestal_Button.grid(row=5, column=0)

        # Burst Slope Setting
        # -----------------
        Burst_Slope_Text = Label(self, text="Burst Slope",font = 14)
        Burst_Slope_Text.grid(row=8, column=0)

        self.Burst_Slope_Value = IntVar()
        self.Burst_Slope_Scale = Scale(self, from_=0, to_=255,variable = self.Burst_Slope_Value ,orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
        self.Burst_Slope_Scale.grid(row=9, column=0)

        # Button Burst Slope sending
        Burst_Slope_Button = Button(self, text="Set Slope", command=lambda : Update_Burst_Slope(self),activebackground="LightSteelBlue3",font = 14)
        Burst_Slope_Button.grid(row=10, column=0)

        # Manual User Input
        Pedestal_Usr_Entry = Entry(self, font=14, width=3)
        Pedestal_Usr_Entry.grid(row=4, column=2)
        Pedestal_Usr_Button = Button(self, font=14, text="Change Pedestal",
                                  command=lambda: Change_Slider_Value_From_Input(Pedestal_Usr_Entry, self.Pedestal_Scale,"Pedestal"))
        Pedestal_Usr_Button.grid(row=5, column=2)

        Burst_Slope_Usr_Entry = Entry(self, font=14, width=3)
        Burst_Slope_Usr_Entry.grid(row=9, column=2)
        Burst_Slope__Usr_Button = Button(self, font=14, text="Change Burst Slope",
                                     command=lambda: Change_Slider_Value_From_Input(Burst_Slope_Usr_Entry,
                                                                                    self.Burst_Slope_Scale,"Burst Slope"))
        Burst_Slope__Usr_Button.grid(row=10, column=2)

        # Help Menu Buttons
        Pedestal_Help_Button = Button(self,text = "Get Param Help",command = lambda:Pedestal_Help_Pic(self),font=14).grid(row = 4, column =3)
        Burst_Slope_Help_Button  = Button(self,text = "Get Param Help",command = lambda:Burst_Slope_Help(self),font=14).grid(row = 9, column =3)
          
        #Read Data from Registers
        Pedestal_Value = Read_from_Register(0x07,Register_Selection.get("Pedestal") )
        Burst_Slope_Value = Read_from_Register(0x07, Register_Selection.get("Burst Slope"))
           
        #Set Slider Values
        self.Pedestal_Scale.set(Pedestal_Value)
        self.Burst_Slope_Scale.set(Burst_Slope_Value)
           

    def Set_Pedestal_Value_From_Register(self):
        Pedestal_Value = Read_from_Register(0x07,Register_Selection.get("Pedestal") )
        self.Pedestal_Scale.set(Pedestal_Value)
    
    def Set_Burst_Slope_Value_From_Register(self):
        Burst_Slope_Value = Read_from_Register(0x07, Register_Selection.get("Burst Slope"))
        self.Burst_Slope_Scale.set(Burst_Slope_Value)
        
    def Get_Pedestal_Scale_Value(self):
        return self.Pedestal_Scale.get()
    
    def Get_Burst_Slope_Scale_Value(self):
        return self.Burst_Slope_Scale.get()
    
    def Set_Pedestal_Scale_Value(self,value):
        if value > 255:
            value = 255
        self.Pedestal_Scale.set(value)
    
    def Set_Burst_Slope_Scale_Value(self,value):
        if value > 255:
            value = 255
        self.Burst_Slope_Scale.set(value)
          
                
class Page_5(Frame):

    def __init__(self, parent, controller):
        Frame.__init__(self, parent)
        self.controller = controller
        
        global query
        
        # Hsync-p-toffset
        # ----------------
        Hsync_Offset_Text = Label(self, text="Hsync Offset [40ns/lsb]",font = 14)
        Hsync_Offset_Text.grid(row=3, column=0, pady=2)


        self.Hsync_Value = IntVar()
        self.Hsync_Offset_Scale = Scale(self, from_=0, to_=32, orient=HORIZONTAL, length=300,sliderlength = 50,width = 30)
        self.Hsync_Offset_Scale.grid(row=4, column=0, pady=2)

        # Button for Hsync
        Hsync_Offset_Button = Button(self, text="Set offset", command=lambda : Update_Hsync(self),activebackground="LightSteelBlue3",font = 14)
        Hsync_Offset_Button.grid(row=5, column=0, pady=2)

        # Manual User Input
        Hsync_Usr_Entry = Entry(self, font=14, width=2)
        Hsync_Usr_Entry.grid(row=4, column=2)
        Hsync_Usr_Button = Button(self, font=14, text="Change Hsync Offset",
                                     command=lambda: Change_Slider_Value_From_Input(Hsync_Usr_Entry, self.Hsync_Offset_Scale,"Hsync"))
        Hsync_Usr_Button.grid(row=5, column=2)

        Hsync_Help_Button = Button(self, text="Get Param Help", command=lambda: Hsync_Pic(self), font=14).grid(row=4,
                                                                                                               column=3)
        #Read Data from Registers
        Hsync_Value = Read_from_Register(0x07,Register_Selection.get("HSync"))
        #Set Slider Values
        self.Hsync_Offset_Scale.set(Hsync_Value)
        
        query += "\n All Register Parameters are updated"
    
    def Set_HSync_Value_from_Register(self):
        Hsync_Value = Read_from_Register(0x07,Register_Selection.get("HSync"))
        #Set Slider Values
        self.Hsync_Offset_Scale.set(Hsync_Value)


    def Get_HSync_Scale_Value(self):
        return self.Hsync_Offset_Scale.get()
    def Set_HSync_Scale_Value(self,value):
        if value > 31:
            value = 31
        self.Hsync_Offset_Scale.set(value)

class Page_6(Frame):
      
      def __init__(self, parent, controller):
        Frame.__init__(self, parent)
        self.controller = controller
                                      
        #Read or Write to Register 
#-------------------------------------------------------------------------------------
        Text_Manual=Label(self,text="Read from Register",font=14)
        Text_Manual.grid(row=1,column=1)

        Register_Group_Input = Entry(self,font=14)
        Register_Group_Input.grid(row=2,column=1)
        Register_Group_Input.insert(0,"Source Registerbank")

        Register_Input = Entry(self,font=14)
        Register_Input.grid(row=3,column=1)
        Register_Input.insert(0,"Source Register")

        Get_Reg_Content_Button = Button(self,text="Get Content",command=lambda : Get_Reg_Content(Register_Group_Input,Register_Input,Reg_Content_Entry),font=14)
        Get_Reg_Content_Button.grid(row=4,column=1)

        Reg_Content_Entry = Entry(self,font=14)
        Reg_Content_Entry.grid(row = 5, column = 1)
        Reg_Content_Entry.insert(0,"Register Content")

        #Write to register x
        Text_Write_Manual=Label(self,text="Write to Register",font=14)
        Text_Write_Manual.grid(row=6,column=1)

        Register_Group_W_Input = Entry(self,text="Reg",font=14)
        Register_Group_W_Input.grid(row=7,column=1)
        Register_Group_W_Input.insert(0,"Source Registerbank")

        Register_to_Write =Entry(self,font=14)
        Register_to_Write.grid(row=8,column=1)
        Register_to_Write.insert(0,"Source Register")

        Data_to_Write = Entry(self,font=14)
        Data_to_Write.grid(row=9,column=1)
        Data_to_Write.insert(0,"Data for Register")

        Get_Reg_Content_Button = Button(self,text="Write Content",command=lambda :
                                        Write_to_Register(Register_Group_W_Input,Register_to_Write,Data_to_Write),font=14)
        Get_Reg_Content_Button.grid(row=10,column=1)

#"main"




def main():
        global query

        Bootup_Check()
        query+="\nReading current Register Values"


        Gui_Object = Testpicture_Gen()
        Gui_Object.mainloop()
query = ""
if __name__ == "__main__":


    main()