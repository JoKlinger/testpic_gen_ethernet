"""
    Created: 07/04/2020
    :author JoKl
    :company iPEK

    The PC9030K Modul contains the TCP Communication in this case
"""


"""
Registers are devided into Group A to H  (p.57)
Selection by:
Slave Adress - 0x03 - Register
with
Register:
Group A    0x00
Group B    0x01
Group C    0x02
Group D    0x03
Group E    0x04
Group F    0x05
Group G    0x06
Group H    0x07
"""
# Dictionary for Group Selection (needed For open Config Menu
Group_Selection = {"Group A": 0x00, "Group B": 0x01, "Group C": 0x02, "Group D": 0x03, "Group E": 0x04, "Group F": 0x05,
                   "Group G": 0x06, "Group H": 0x07}
# dictionary for Registers
Register_Selection = {"Sync Rise Time": 0xAF, "Burst Offset": 0xE6, "Encad Rise Time": 0xEB, "Setup Width": 0xA9,
                      "Pedestal": 0xBB,"Burst Slope":0xAC,"HSync":170}
from Remote_Control import *



def Read_from_Register(Registerbank, Register):
    Registergroup = Group_Selection.get("Group H")
    Register_Content = Read_from_Client(Registergroup, Register,0x02,0x33)
    return int(Register_Content)

def Mirror_Display(i2c_adress, State):
    print("Mirror Display")
    if State == True:  # Mirror Display
        Write_to_Client(Group_Selection.get("Group H"), 0x05, 0x01,i2c_adress)
    else:
        Write_to_Client(Group_Selection.get("Group H"), 0x05, 0x00,i2c_adress)


def Flip_Display(i2c_adress, State):
    print("Flip Display")
    if State == True:  # Flip Display
        Write_to_Client(Group_Selection.get("Group H"), 0x05, 0x02,i2c_adress)
    else:
        Write_to_Client(Group_Selection.get("Group H"), 0x05, 0x00,i2c_adress)

# -----------------------------------------------------------------
# Update Register Values

def Update_Sync_Risetime(frame_object):
    Sync_Value = frame_object.Get_Sync_Rising_Scale_Value()
    print("Update Sync Risetime ", Sync_Value)
    Write_to_Client( Group_Selection.get("Group H"), 0xAF, Sync_Value,0x33)
def Update_Burst_Slope(frame_object):
    Burst_Slope_value = frame_object.Get_Burst_Slope_Scale_Value()
    print("Update Burst Slope: ", Burst_Slope_value)
    Write_to_Client( Group_Selection.get("Group H"), 0xAC, Burst_Slope_value,0x33)

# Burst Offset MSB Defines Direction other bits for Magnitude
def Update_Burst_offset(frame_object):
    Value = frame_object.Get_Burst_Offset_Scale_Value()
    if Value < 0:
        Value = abs(Value) | (1 << 7)  # If Checkbox is set shift to the left == Set MSB to 1
    print("Update Burst Offset", Value)
    Write_to_Client( Group_Selection.get("Group H"), 0xE6, Value,0x33)

#write Current slider value to Register
def Update_Setup_Width(frame_object):
    value = frame_object.Get_Setup_Width_Scale_Value()
    print("Update Setup Width ", value)
    Write_to_Client( Group_Selection.get("Group H"), 0xA9, value,0x33)

#write Current slider value to Register
def Update_Encad_Rising(frame_object):
    Value = frame_object.Get_Encad_Rising_Scale_Value()
    print("Encad Rising Update ", Value)
    Write_to_Client( Group_Selection.get("Group H"), 0xEB, Value,0x33)

#write Current slider value to Register
def Update_Pedestal(frame_object):
    Value = frame_object.Get_Pedestal_Scale_Value()
    print("Pedestal Update ", Value)
    Write_to_Client( Group_Selection.get("Group H"), 0xBB, Value,0x33)

#write Current slider value to Register
def Update_Hsync(frame_object):
    print("Updating")
    Register_Value = frame_object.Get_HSync_Scale_Value()
    Write_to_Client( Group_Selection.get("Group H"), 170, Register_Value,0x33)

# -----------------------------------------------------------------
def Write_Test_Pattern(Test_Pattern_Number):
    print("Testpattern", Test_Pattern_Number)
    # Sat Kamera
    if Test_Pattern_Number == 0:
         Write_to_Client(Group_Selection.get("Group H"), 0x0C, 0x00,0x33)

    if Test_Pattern_Number > 0 and Test_Pattern_Number < 11:
        Write_to_Client(Group_Selection.get("Group H"), 0x0C, Test_Pattern_Number,0x33)


    # need to add 3 as offset (Testpattern not continous)
    elif Test_Pattern_Number > 10 and Test_Pattern_Number <= 27:
        Write_to_Client( Group_Selection.get("Group H"), 0x0C, Test_Pattern_Number,0x33)



