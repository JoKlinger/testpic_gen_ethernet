"""
Created: 07/04/2020
:author: JoKl
:company: iPEK

Function:
TCP/IP is implemented as Thread which runs independent, gets called by TCP/Ip remote Interface

"""

from threading import Thread
import threading
from PC9030K import *

#class for splitted data set
class Data_Formatting():
    """ This class splits the Data from the TCP-Stream into it's components:
        Frame: Identifier, Register_Group, Adress, Value, i2c_Adress ;
        The Identifier can be 0x01 for writing or 0x02 for reading
    """
    def __init__(self):
        """ Class Constructor
        :param self.stream: input stream which gets decoded from the TCP (string)
        :param self.Register_Group: Register_Group for Register (0x00-0x07)
        :param self.Register: Register to access /accessed register
        :param self.Register_Value: Value for Register
        :param self.I2C_Adress: I2C Adress used for Communication
        :param self.Response_Expected: write back to TCP remote connection
        """

        self.stream = ""
        self.Register_Group = 0x00
        self.Register = 0x00
        self.Register_Value = 0x00
        self.I2C_Adress = 0x00
        self.Responce_Expected = False

    def Seperate_Content(self):
        """Seperates the TCP Stream and calls the functions which writes the Register content to the specified register
        """

        Splitted = self.stream.split(",")        
        
        #print("Stream:{}".format(Splitted))
        
        try:
            self.Responce_Expected=int(Splitted[0])
            self.Register_Group=int(Splitted[1])
            self.Register=int(Splitted[2])
            self.Register_Value=int(Splitted[3])
            self.I2C_Adress = Splitted[4]
            print("Responce Expected :{}\n Register Group: {}\nRegister: {}\nRegister Value: {}\n".format(self.Responce_Expected,self.Register_Group,self.Register,self.Register_Value))
            if self.Responce_Expected == 0x01:
                self.Write_Content()
            
        except ValueError:
                print ("Data Type not valid")

    def Validate_Input(self):
        pass
        
    def Write_Content(self):
        """Writes the Register Content
        """
        Write_Register(self.Register_Group, self.Register, self.Register_Value)
        
    def Readback_Register(self):
        """Readback Register Content to make sure that the data has been written
        """
        Reg_Value = Read_from_Register(self.Register_Group,self.Register)
        print("TCP Register Value Writeback:{}".format(Reg_Value))
        return (str(2)+","+str(self.Register_Group)+","+str(self.Register)+","+str(Reg_Value)+","+str(self.I2C_Adress)+";")


class TCP_Thread(Thread):
    """Thread Class which handles the Independent TCP/IP Functionality, 
    """

    def __init__(self,gui_object,socket_object,IP_Adress,byte_count):
        """ Constructor for TCP Thread
        :param self.g_object = GUI Object, to access GUI Parameters like Slider Positions
        :param self.s_object = Socket object, passed by main application
        :param IP_Adress = IP_Adress, passed by main Application (Entry inside the Menubar)
        :param byte_count = not used at the moment
        """
        print("Constructor called")
        super().__init__()
        self.g_object=gui_object
        self.s_object=socket_object
        self.IP_Adress=IP_Adress
        self.payload = byte_count


        self.s_object.bind(("",50000))
        self.s_object.listen(1)#passive listening

    def run(self):
        """ Thread Run function, endless loop, exit if the TCP/IP connetion gets closed
        """
        print("Running")
        Reg_Data=Data_Formatting()
        Data_Packet = ""
        try:
            while True:
                comm,add =self.s_object.accept()
                while True:
                    print("Received")
                    Data_Packet=comm.recv(1024).decode() #self.payload
            
                    if ";" in Data_Packet: #full Payload received
                        print("Data Packet received")
                        Data_Packet.replace(";","")#Remove end detection
                        Reg_Data.stream = Data_Packet
                        print("Received Stream:",Reg_Data.stream)
                        Reg_Data.Seperate_Content()
                        response = Reg_Data.Readback_Register()
                        #print("Writing Back:"+response)
                        comm.send(response.encode())
                        
                    if not Data_Packet:
                        comm.close()
                        break
            #print("IP_Adress:[{}],Data:{}".format(add,Data_Packet))
        finally:
            self.s_object.close()
