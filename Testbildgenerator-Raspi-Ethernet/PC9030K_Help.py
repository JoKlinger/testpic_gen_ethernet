from tkinter import *
from PIL import ImageTk,Image

def Show_Testpicture_Help(root,which):

    window1 = Toplevel(root)
    window1.title("Testpicture Selections")
    window1.geometry("800x400")
    
    if which == 1:
        data = Image.open("Testpictures_1.png")
    else:
        data = Image.open("Testpictures_2.png")
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack()

def Show_Burst_Offset_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Burst Help Menu")
    window1.geometry("800x400")
    
    data = Image.open("Burst_Settings.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack()

def Show_Rising_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Sync Rising Help")
    window1.geometry("800x400")
    
    data = Image.open("Sync_Rising.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack() 

def Show_Encad_Rising_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Encad Rising Help")
    window1.geometry("800x400")
    
    data = Image.open("Encad_Rising.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack()
    
def Show_Setup_Width_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Setup Width Help")
    window1.geometry("800x400")
    
    data = Image.open("Setup_Width.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack() 

def Show_Pedestal_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Pedestal Help")
    window1.geometry("800x400")
    
    data = Image.open("Enc_Pedestal.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack() 

def Show_Burst_Slope_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Pedestal Help")
    window1.geometry("800x400")
    
    data = Image.open("Burst_Duration_Burst_Slope.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack()
    
def Show_Hsync_Pic_Help(root):
    
    window1 = Toplevel(root)
    window1.title("Pedestal Help")
    window1.geometry("800x400")
    
    data = Image.open("Burst_Duration_Burst_Slope.PNG")
    
    data.thumbnail([800,400],Image.ANTIALIAS)
    photo = ImageTk.PhotoImage(data)
    label = Label(window1,image=photo)
    label.image = photo # keep a reference!
    label.pack()
  