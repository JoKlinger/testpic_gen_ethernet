import smbus

""" 
Registers are devided into Group A to H  (p.57)
Selection by:
Slave Adress - 0x03 - Register
with
Register:
Group A    0x00
Group B    0x01
Group C    0x02
Group D    0x03
Group E    0x04
Group F    0x05
Group G    0x06
Group H    0x07
"""
# Dictionary for Group Selection (needed For open Config Menu
Group_Selection = {"Group A": 0x00, "Group B": 0x01, "Group C": 0x02, "Group D": 0x03, "Group E": 0x04, "Group F": 0x05,
                   "Group G": 0x06, "Group H": 0x07}
# dictionary for Registers
Register_Selection = {"Sync Rise Time": 0xAF, "Burst Offset": 0xE6, "Encad Rise Time": 0xEB, "Setup Width": 0xA9,
                      "Pedestal": 0xBB,"Burst Slope":0xAC,"HSync":170}


def Write_Register(Register_Bank, Register, Register_Value):
    bus_object = smbus.SMBus(1)
    i2c_adress = 0x33
    bus_object.write_i2c_block_data(i2c_adress, 0x03, [Register_Bank])
    bus_object.write_i2c_block_data(i2c_adress, Register, [Register_Value])
    bus_object.close()
    
def Read_from_Register(Registerbank, Register):
    bus_object = smbus.SMBus(1)
    i2c_adress = 0x33
    bus_object.write_i2c_block_data(i2c_adress, 0x03, [Registerbank])
    Register_Content = bus_object.read_byte_data(i2c_adress, Register)
    bus_object.close()
    return Register_Content

def Mirror_Display(i2c_adress, bus_object, State):
    print("Mirror Display")
    if State == True:  # Mirror Display
        bus_object.write_i2c_block_data(0x33, 0x03, [0x00])
        bus_object.write_i2c_block_data(0x33, 0x05, [0x01])
    else:
        bus_object.write_i2c_block_data(0x33, 0x03, [0x00])
        bus_object.write_i2c_block_data(0x33, 0x05, [0x00])


def Flip_Display(i2c_adress, bus_object, State):
    print("Flip Display")
    if State == True:  # Flip Display
        bus_object.write_i2c_block_data(0x33, 0x03, [0x00])
        bus_object.write_i2c_block_data(0x33, 0x05, [0x02])
    else:
        bus_object.write_i2c_block_data(0x33, 0x03, [0x00])
        bus_object.write_i2c_block_data(0x33, 0x05, [0x00])

def Set_Burst_TOffset(i2c_adress, bus_object, Value):
    bus_object.write_i2c_block_data(i2c_adress, 0x03, [0x07])
    bus_object.write_i2c_block_data(i2c_adress, 0xE6, [Value])
# -----------------------------------------------------------------
# Update Register Values

def Update_Sync_Risetime(frame_object):
    Sync_Value = frame_object.Get_Sync_Rising_Scale_Value()
    print("Update Sync Risetime ", Sync_Value)
    Write_Register(0x07, 0xAF, Sync_Value)

    
def Update_Burst_Slope(frame_object):
    Burst_Slope_value = frame_object.Get_Burst_Slope_Scale_Value()
    print("Update Burst Slope: ", Burst_Slope_value)
    Write_Register(0x07, 0xAC, Burst_Slope_value)

# Burst Offset MSB Defines Direction other bits for Magnitude
def Update_Burst_offset(frame_object):
    Value = frame_object.Get_Burst_Offset_Scale_Value()
    
    if Value < 0:
        Value = abs(Value) | (1 << 7)  # If Checkbox is set shift to the left == Set MSB to 1

    print("Update Burst Offset", Value)
    bus_object = smbus.SMBus(1)
    i2c_adress = 0x33
    Set_Burst_TOffset(i2c_adress, bus_object, Value)
    bus_object.close()

#write Current slider value to Register
def Update_Setup_Width(frame_object):
    value = frame_object.Get_Setup_Width_Scale_Value()
    print("Update Setup Width ", value)
    Write_Register(0x07, 0xA9, value)

#write Current slider value to Register
def Update_Encad_Rising(frame_object):
    Value = frame_object.Get_Encad_Rising_Scale_Value()
    print("Encad Rising Update ", Value)
    Write_Register(0x07, 0xEB, Value)

#write Current slider value to Register
def Update_Pedestal(frame_object):
    Value = frame_object.Get_Pedestal_Scale_Value()
    print("Pedestal Update ", Value)
    Write_Register(0x07, 0xBB, Value)

#write Current slider value to Register
def Update_Hsync(frame_object):
    print("Updating")
    Register_Value = frame_object.Get_HSync_Scale_Value()
    Write_Register(0x07, 170, Register_Value)


# -----------------------------------------------------------------
def Write_Test_Pattern(Test_Pattern_Number):
    bus_object = smbus.SMBus(1)
    i2c_adress = 0x33

    print("Testpattern", Test_Pattern_Number)

    # Sat Kamera
    if Test_Pattern_Number == 0:
        bus_object.write_i2c_block_data(i2c_adress, 0x03,
                                        [0x02])  # i2c Adresse - Register Group Selection -Register Group
        bus_object.write_i2c_block_data(i2c_adress, 0x0C, [0x00])

    if Test_Pattern_Number > 0 and Test_Pattern_Number < 11:
        bus_object.write_i2c_block_data(i2c_adress, 0x03, [0x02])
        bus_object.write_i2c_block_data(i2c_adress, 0x0C, [Test_Pattern_Number])


    # need to add 3 as offset (Testpattern not continous)
    elif Test_Pattern_Number > 10 and Test_Pattern_Number <= 27:
        bus_object.write_i2c_block_data(i2c_adress, 0x03, [0x02])
        bus_object.write_i2c_block_data(i2c_adress, 0x0C, [Test_Pattern_Number + 2])

    bus_object.close()

